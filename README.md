# [`text2map.theme`](https://culturalcartography.gitlab.io/text2map.theme/): Theme for ggplot2 <img src="man/figures/logo.png" align="right" height="120" />

This is an R Package for customizing the `ggplot2` aesthetics. See also [`text2map`](https://culturalcartography.gitlab.io/text2map/).

### Installation and Usage

``` r
library(remotes)
install_gitlab("culturalcartography/text2map.theme")

text2map.theme::set_theme()

```

## Related Packages

There are four related packages hosted on GitLab: 

- [`text2map`](https://culturalcartography.gitlab.io/text2map): text analysis functions
- [`text2map.corpora`](https://culturalcartography.gitlab.io/text2map.corpora/): 13+ text datasets
- [`text2map.dictionaries`](https://culturalcartography.gitlab.io/text2map.dictionaries/): norm dictionaries and word frequency lists
- [`text2map.pretrained`](https://culturalcartography.gitlab.io/text2map.pretrained/): pretrained embeddings and topic models

The above packages can be installed using the following:

```r
install.packages("text2map")

library(remotes)
install_gitlab("culturalcartography/text2map.corpora")
install_gitlab("culturalcartography/text2map.pretrained")
install_gitlab("culturalcartography/text2map.dictionaries")
```
# Contributions and Support

If you have any suggestions to improve this theme -- especially for accessibility --
send us an email (maintainers [at] textmapping.com) or submit pull requests.

Please report any issues or bugs here: https://gitlab.com/culturalcartography/text2map.theme/-/issues